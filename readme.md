
# ov-curriculum-api

OV Curriculum API provides the curriculum content to the OneValley passport.

## Setup

```bash
npm install

npm run start
```

The API runs with the following default configuration

```bash
# port the api runs on
API_PORT="8000"
```

Custom configuration can be loaded by creating a `.env` in the root directory of the project. `.env.example` provides the sample config for the reference.