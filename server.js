require('dotenv').config();

const port = process.env.API_PORT || 8000;

const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors());

app.get('/', (req, res) => {
  console.log('ov-curriculum-api HIT!');
  res.json({ message: 'Welcome to the ov-curriculum-api.' });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}.`);
});
